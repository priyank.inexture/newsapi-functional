import React, { createContext, useReducer, useContext } from "react";
const GlobalStateContext = createContext();
const GlobalDispatchContext = createContext();

const globalReducer = (state, action) => {
  //   console.log(state, action);
  switch (action.type) {
    case "UPDATE_ARTICLES":
      return {
        ...state,
        articles: action.value,
      };
    case "UPDATE_SELECTED_ARTICLE":
      return {
        ...state,
        selectedArticle: action.load,
      };
    case "UPDATE_PREV_PAGE":
      return {
        ...state,
        lastPage: action.value,
      };
    case "UPDATE_PREV_COUNTRY":
      return {
        ...state,
        lastCountry: action.value,
      };
    default:
      throw new Error("No reduce for this action");
  }
};
const initialState = {
  articles: [],
  lastPage: 1,
  lastCountry: "",
};
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(globalReducer, initialState);
  return (
    <GlobalDispatchContext.Provider value={dispatch}>
      <GlobalStateContext.Provider value={state}>
        {children}
      </GlobalStateContext.Provider>
    </GlobalDispatchContext.Provider>
  );
};
export const useGlobalStateContext = () => useContext(GlobalStateContext);
export const useGlobalDispatchContext = () => useContext(GlobalDispatchContext);
