import React from "react";
import {
  useGlobalStateContext,
  // useGlobalDispatchContext,
} from "./context/globalContext";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Layout } from "antd";
import ArticleList from "./components/ArticleList";
import ArticleDetail from "./components/ArticleDetail";
//css
import "antd/dist/antd.css";

function App() {
  const { articles,lastPage,lastCountry } = useGlobalStateContext();  
  return (
    <Layout className="layout">
      <Router>
        <Switch>
          <Route path="/detail">
            <ArticleDetail/>
          </Route>
          <Route path="/">
            <ArticleList articles={articles} propPage={lastPage} propCountry={lastCountry}/>
          </Route>
        </Switch>
      </Router>
    </Layout>
  );
}

export default App;
