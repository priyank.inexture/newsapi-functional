export const formateDate = (dateString) => {
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    return new Date(dateString).toLocaleDateString("en-US", options);
  };
  export const countries = {
    ae: "The United Arab Emirates",
    ar: "Argentina",
    at: "Austria",
    au: "Australia",
    be: "Belgium",
    bg: "Bulgaria",
    br: "Brazil",
    ca: "Canada",
    ch: "Switzerland",
    cn: "China",
    co: "Colombia",
    cu: "Cuba",
    cz: "Czechia",
    de: "Germany",
    eg: "Egypt",
    fr: "France",
    gb: "United Kingdom",
    gr: "Greece",
    hk: "Hong Kong",
    hu: "Hungary",
    id: "Indonesia",
    ie: "Ireland",
    il: "Israel",
    in: "India",
    it: "Italy",
    jp: "Japan",
    kr: "South Korea",
    lt: "Lithuania",
    lv: "Latvia",
    ma: "Morocco",
    mx: "Mexico",
    my: "Malaysia",
    ng: "Nigeria",
    nl: "Netherlands",
    no: "Norway",
    nz: "New Zealand",
    ph: "Philippines",
    pl: "Poland",
    pt: "Portugal",
    ro: "Romania",
    rs: "Serbia",
    ru: "Russia",
    sa: "South Africa",
    se: "Sweden",
    sg: "Singapore",
    si: "Slovenia",
    sk: "Slovakia",
    th: "Thailand",
    tr: "Turkey",
    tw: "Taiwan",
    ua: "Ukraine",
    us: "United States",
    ve: "Venezuela",
  };
  export const dummyData = {
    status: "ok",
    totalResults: 38,
    articles: [
      {
        source: {
          id: "cbs-news",
          name: "CBS News",
        },
        author: "Stephen Gandel",
        title:
          "GameStop shares plunge below $100 as WallStreetBets traders face huge losses - CBS News",
        description:
          "SEC has said it's investigating whether brokers acted responsibly and whether the video game retailer's shares were manipulated.",
        url: "https://www.cbsnews.com/news/gamestop-stock-down-gme-losses/",
        urlToImage:
          "https://cbsnews2.cbsistatic.com/hub/i/r/2021/01/30/36fb9d54-c150-4fb8-9c33-0a4374c154ca/thumbnail/1200x630/687fbe2420f49341f16a955b2994451a/cbsn-fusion-stock-market-slips-as-gamestop-shares-soar-thumbnail-636354-640x360.jpg",
        publishedAt: "2021-02-03T12:02:00Z",
        content:
          "GameStop's stock price cratered on Tuesday, falling 60% to $90 a share. The drop signaled that the popular WallStreetBets Reddit stock market discussion board — a major force behind last week's spect… [+3485 chars]",
      },
      {
        source: {
          id: "cnn",
          name: "CNN",
        },
        author: "Caroline Kelly, CNN",
        title:
          "Biden: No family members will be involved in any government decisions - CNN",
        description:
          "President Joe Biden vowed that no one in his extended family will have sway over government decisions in an interview with People magazine published Wednesday.",
        url:
          "https://www.cnn.com/2021/02/03/politics/biden-people-magazine-family-members-government/index.html",
        urlToImage:
          "https://cdn.cnn.com/cnnnext/dam/assets/210202205019-joe-biden-family-super-tease.jpg",
        publishedAt: "2021-02-03T11:02:00Z",
        content:
          "(CNN)President Joe Biden vowed that no one in his extended family will have sway over government decisions in an interview with People magazine published Wednesday.\r\n\"We're going to run this like the… [+4033 chars]",
      },
      {
        source: {
          id: null,
          name: "New York Times",
        },
        author: "Hannah Beech",
        title:
          "After Coup, Myanmar Military Charges Aung San Suu Kyi With Obscure Infraction - The New York Times",
        description:
          "The move against the deposed civilian leader was a curious coda to the army’s rapid dismantling of the country’s nascent democracy.",
        url:
          "https://www.nytimes.com/2021/02/03/world/asia/myanmar-coup-aung-san-suu-kyi.html",
        urlToImage:
          "https://static01.nyt.com/images/2021/02/03/world/03myanmar3/merlin_183173709_98a2ba96-ff68-447a-9670-259d0130838e-facebookJumbo.jpg",
        publishedAt: "2021-02-03T11:02:00Z",
        content:
          "But if such crimes seem absurd, they carry real consequences. Along with Ms. Aung San Suu Kyi, President U Win Myint, one of her political acolytes who was also detained on Monday, was issued a deten… [+1774 chars]",
      },
      {
        source: {
          id: "ign",
          name: "IGN",
        },
        author: "Joe Skrebels",
        title: "PS5 System Update Fixes PS4 Disc Installation Bug - IGN - IGN",
        description:
          "A new PS5 system update has resolved an issue that caused PS4 game discs to attempt to install the last-gen version of a game, even if it had already been upgraded.",
        url:
          "https://www.ign.com/articles/ps5-system-update-fixes-ps4-disc-installation-bug",
        urlToImage:
          "https://assets1.ignimgs.com/2020/07/13/ps5blogroll-1594666740705.png?width=1280",
        publishedAt: "2021-02-03T10:43:58Z",
        content:
          "A new PS5 system update has resolved an issue that caused PS4 game discs to attempt to install the last-gen version of a game, even if it had already been upgraded.The 20.02-02.50.00 update includes … [+1454 chars]",
      },
      {
        source: {
          id: "the-wall-street-journal",
          name: "The Wall Street Journal",
        },
        author: "Eric Morath",
        title:
          "Biden Wants a $15 Minimum Wage. Here’s What People Say It Would Do to the Economy - The Wall Street Journal",
        description:
          "White House says a raise would help workers; opponents warn of job cuts",
        url:
          "https://www.wsj.com/articles/biden-wants-a-15-minimum-wage-heres-what-people-say-it-would-do-to-the-economy-11612348201",
        urlToImage: "https://images.wsj.net/im-293885/social",
        publishedAt: "2021-02-03T10:30:00Z",
        content:
          "President Biden says his proposal to raise the federal minimum wage to $15 an hour will lift many low-wage workers out of poverty, but some businesses and economists warn it could cost jobs as the U.… [+8518 chars]",
      },
      {
        source: {
          id: null,
          name: "NPR",
        },
        author: "",
        title:
          "Herd Immunity For COVID-19 Won't Be Easy To Reach : Shots - Health News - NPR",
        description:
          "At least 70% of people will need to be immune from the coronavirus before COVID-19 can recede through a process known as herd immunity. Vaccines can play a role. But reaching the goal won't be easy.",
        url:
          "https://www.npr.org/sections/health-shots/2021/02/03/963373971/a-rocky-road-on-the-way-to-herd-immunity-for-covid-19",
        urlToImage:
          "https://media.npr.org/assets/img/2021/02/02/denver_vax_gettyimages-1230878459_wide-c4ade5e9cad75b81959e654c9c3024adcaefc6d3.jpg?s=1400",
        publishedAt: "2021-02-03T10:16:00Z",
        content:
          "People line up for drive-through COVID-19 vaccination at Coors Field baseball stadium in Denver on Saturday.\r\nChet Strange/AFP via Getty Images\r\nScientists estimate that somewhere between 70% and 85%… [+4955 chars]",
      },
      {
        source: {
          id: null,
          name: "New York Post",
        },
        author: "Associated Press",
        title:
          "Single dose of AstraZeneca vaccine drastically cuts COVID-19 transmission: UK study - New York Post ",
        description:
          "Britain’s health chief says a new study showing that a single dose of the COVID-19 vaccine developed by Oxford University and AstraZeneca provides a high level of protection for 12 weeks supports t…",
        url:
          "https://nypost.com/2021/02/03/astrazeneca-vaccine-drastically-cuts-transmission-of-covid-19-study-finds/",
        urlToImage:
          "https://nypost.com/wp-content/uploads/sites/2/2021/02/4EB2D417-5312-4747-85F6-BE9E3D47894E.jpeg?quality=90&strip=all&w=1200",
        publishedAt: "2021-02-03T10:05:00Z",
        content:
          "Britains health chief says a new study showing that a single dose of the COVID-19 vaccine developed by Oxford University and AstraZeneca provides a high level of protection for 12 weeks supports the … [+1508 chars]",
      },
      {
        source: {
          id: null,
          name: "CNBC",
        },
        author: "Silvia Amaro",
        title:
          "Former ECB chief Mario Draghi on track to be Italy’s next prime minister - CNBC",
        description:
          "Famed for rescuing the euro in the grips of a sovereign debt crisis, Mario Draghi is now being asked to help his own country.",
        url:
          "https://www.cnbc.com/2021/02/03/draghi-former-ecb-president-could-be-italy-new-prime-minister.html",
        urlToImage:
          "https://image.cnbcfm.com/api/v1/image/106834460-1612342394140-gettyimages-1216095076-AFP_1SH62T.jpeg?v=1612342427",
        publishedAt: "2021-02-03T10:00:00Z",
        content:
          "Italian economist and former President of the European Central Bank, Mario Draghi.\r\nLONDON Famed for rescuing the euro in the grips of a sovereign debt crisis, Mario Draghi is now being asked to help… [+4409 chars]",
      },
      {
        source: {
          id: null,
          name: "Daily Mail",
        },
        author: "Rebecca Lawrence",
        title:
          "Kylie Jenner and Travis Scott 'are still madly in love' and 'haven't ruled out reconciling' - Daily Mail",
        description:
          "They called an end to their two-year romance back in 2019.  Yet Kylie Jenner and Travis Scott are allegedly considering reconciling amid reports the pair are 'still madly in love'.",
        url:
          "https://www.dailymail.co.uk/tvshowbiz/article-9218353/Kylie-Jenner-Travis-Scott-madly-love-havent-ruled-reconciling.html",
        urlToImage:
          "https://i.dailymail.co.uk/1s/2021/02/03/10/38824024-0-image-a-39_1612346431310.jpg",
        publishedAt: "2021-02-03T10:00:00Z",
        content:
          "Reality star and makeup mogul Kylie Jenner and rapper Travis Scott are currently 'taking a break' from their relationship.\r\nTMZ's report revealed that they will continue to co-parent their 19-month o… [+7284 chars]",
      },
      {
        source: {
          id: "fox-news",
          name: "Fox News",
        },
        author: "Jack Durschlag",
        title:
          "FBI agents' slayings in Florida draw Biden's grief: 'A hell of a price to pay' - Fox News",
        description: "",
        url:
          "https://www.foxnews.com/us/fbi-agents-slayings-in-florida-draw-bidens-grief-a-hell-of-a-price-to-pay",
        urlToImage:
          "https://static.foxnews.com/foxnews.com/content/uploads/2021/02/FBI-AP-4.jpg",
        publishedAt: "2021-02-03T09:55:29Z",
        content:
          "Good morning and welcome to Fox News First. Here's what you need to know as you start your day ...\r\nFBI agents' slayings in Florida draw Biden's grief: 'A hell of a price to pay'After the first major… [+5916 chars]",
      },
      {
        source: {
          id: null,
          name: "INSIDER",
        },
        author: "Julie Gerstein",
        title:
          "Country singer Morgan Wallen filmed hurling racial slur, apologizes - Insider - Insider",
        description:
          "Wallen was heard yelling a series of comments to a man near his Nashville home, and a neighbor filmed the incident, TMZ reported.",
        url:
          "https://www.insider.com/morgan-wallen-country-singer-filmed-hurling-racial-slur-2021-2",
        urlToImage:
          "https://i.insider.com/601a5f2eee136f00183a9f7f?width=1200&format=jpeg",
        publishedAt: "2021-02-03T09:53:08Z",
        content:
          "Country singer Morgan Wallen was filmed hurling a racial slur on the streets of Nashville on Sunday night, prompting one of the biggest radio chains in the US to drop him from its playlists.\r\nAccordi… [+1916 chars]",
      },
      {
        source: {
          id: "nbc-news",
          name: "NBC News",
        },
        author: "Jane C. Timm",
        title:
          "Election workers weren't surprised by the Capitol riot. Trump's supporters targeted them first. - NBC News",
        description:
          "Election workers who watched a violent pro-Trump mob ransacked the Capitol on Jan. 6 shared experiences of being targeted and harassed for months.",
        url:
          "https://www.nbcnews.com/politics/elections/election-workers-weren-t-surprised-capitol-riot-trump-s-supporters-n1256535",
        urlToImage:
          "https://media3.s-nbcnews.com/j/newscms/2021_04/3446214/210129-election-protest-arizona-ew-331p_d1206da078869e158ffa19799500df28.nbcnews-fp-1200-630.jpg",
        publishedAt: "2021-02-03T09:31:00Z",
        content:
          "Election workers watched along with the rest of America as a violent pro-Trump mob ransacked the Capitol on Jan. 6, leaving five people dead.\r\nThey were horrified and shocked, but not surprised.\r\nI t… [+10847 chars]",
      },
      {
        source: {
          id: null,
          name: "HuffPost",
        },
        author: "Lee Moran",
        title:
          "Jeff Bezos Stepped Down As Amazon CEO And The Gags Came Quicker Than A Prime Package - HuffPost",
        description:
          '"Jeff Bezos delivered his resignation letter in a box that would fit a washing machine," one Twitter user cracked at the announcement.',
        url:
          "https://www.huffpost.com/entry/jeff-bezos-amazon-jokes_n_601a507ec5b6c2d891a46464",
        urlToImage:
          "https://img.huffingtonpost.com/asset/601a5374250000170591adfd.jpeg?cache=n1sC9tOTfS&ops=1200_630",
        publishedAt: "2021-02-03T07:56:00Z",
        content:
          "The gags came fast on Twitter after Jeff Bezos announced he will step down as CEO of Amazon.com, the online juggernaut he started 27 years ago.\r\nBezos — the world’s second-richest person, behind Tesl… [+531 chars]",
      },
      {
        source: {
          id: null,
          name: "Yahoo Entertainment",
        },
        author: "Terez Paylor",
        title:
          "Chiefs' Eric Bieniemy isn't here for a pity party, especially with a 2nd straight Super Bowl within grasp - Yahoo Sports",
        description:
          "If there’s one thing you need to understand about Bieniemy, it’s this: all this media attention touting him to be a head coach isn’t what he asked for.",
        url:
          "https://sports.yahoo.com/chiefs-eric-bieniemy-isnt-here-for-the-pity-parade-especially-with-a-second-straight-super-bowl-within-grasp-044021890.html",
        urlToImage:
          "https://s.yimg.com/ny/api/res/1.2/EmI3nEPl478CQqAI2dgiRw--/YXBwaWQ9aGlnaGxhbmRlcjt3PTIwMDA7aD0xMzM0/https://s.yimg.com/os/creatr-uploaded-images/2021-02/bac72ea0-65d4-11eb-bd5f-31863d4d657b",
        publishedAt: "2021-02-03T07:40:00Z",
        content:
          "If theres one thing you need to understand about Kansas City Chiefs offensive coordinator Eric Bieniemy, its this: all this media attention touting him to be a head coach isnt what he asked for.\r\nAll… [+5920 chars]",
      },
      {
        source: {
          id: null,
          name: "ESPN",
        },
        author: "Lindsey Thiry",
        title:
          "Detroit Lions' Jared Goff 'happy, grateful, ready for a new opportunity' after trade - ESPN",
        description:
          "Detroit Lions quarterback Jared Goff said he's \"happy, grateful, ready for a new opportunity\" after his time with the Rams ended in last week's blockbuster trade.",
        url:
          "https://www.espn.com/nfl/story/_/id/30824989/detroit-lions-jared-goff-happy-grateful-ready-new-opportunity-trade",
        urlToImage:
          "https://a3.espncdn.com/combiner/i?img=%2Fphoto%2F2021%2F0118%2Fr803271_1296x729_16%2D9.jpg",
        publishedAt: "2021-02-03T06:18:55Z",
        content:
          "Quarterback Jared Goff says he's ready to move on in the aftermath of a blockbuster trade that dealt him from the Los Angeles Rams to the Detroit Lions.\r\n\"As the quarterback, as the guy that's at arg… [+3095 chars]",
      },
      {
        source: {
          id: null,
          name: "CNBC",
        },
        author: "Arjun Kharpal",
        title:
          "Jack Ma's uneasy relationship with Beijing casts shadow over Alibaba's strong earnings and future - CNBC",
        description:
          "Jack Ma appears to have got on the wrong side of the Chinese government, sparking a chain of events that has upped regulatory scrutiny on Alibaba.",
        url:
          "https://www.cnbc.com/2021/02/03/jack-ma-tension-with-beijing-casts-shadow-over-alibabas-future.html",
        urlToImage:
          "https://image.cnbcfm.com/api/v1/image/106242320-1573646710816preview-6.jpg?v=1573646722",
        publishedAt: "2021-02-03T05:56:00Z",
        content:
          "HANGZHOU, CHINA - NOVEMBER 13: Alibaba founder Jack Ma attends the 5th World Zhejiang Entrepreneurs Convention at Hangzhou International Expo Centre on November 13, 2019 in Hangzhou, Zhejiang Provinc… [+3403 chars]",
      },
      {
        source: {
          id: null,
          name: "Daily Beast",
        },
        author: "Matt Wilstein",
        title:
          "Jimmy Kimmel Warns Melania Marjorie Taylor Greene Is 'Coming' for Trump - The Daily Beast",
        description:
          "The late-night host joked that the QAnon congresswoman is “distracting Republicans from the important work of blocking COVID relief to millions of Americans who need it.”",
        url:
          "https://www.thedailybeast.com/jimmy-kimmel-warns-melania-marjorie-taylor-greene-is-coming-for-trump",
        urlToImage:
          "https://img.thedailybeast.com/image/upload/c_crop,d_placeholder_euli9k,h_1420,w_2524,x_0,y_0/dpr_2.0/c_limit,w_740/fl_lossy,q_auto/v1612331099/JK_26_klvbm7",
        publishedAt: "2021-02-03T05:46:00Z",
        content:
          "Late-night TVs new favoritepunchingbag took some more hits Tuesday night when Jimmy Kimmel spent the better part of his monologue digging into the latest controversy surrounding QAnon Congresswoman M… [+1122 chars]",
      },
      {
        source: {
          id: "the-verge",
          name: "The Verge",
        },
        author: "Sam Byford",
        title:
          "Huawei’s next folding phone is coming on February 22nd - The Verge",
        description:
          "Huawei says its Mate X2 folding phone will be announced later this month. It should be the first significant followup to the original Mate X.",
        url:
          "https://www.theverge.com/2021/2/3/22263750/huawei-mate-x2-folding-phone-announcement-date",
        urlToImage:
          "https://cdn.vox-cdn.com/thumbor/ZO_2Y3RXgQBhBfWaUT8mO_T2B8M=/0x50:690x411/fit-in/1200x630/cdn.vox-cdn.com/uploads/chorus_asset/file/22277923/matex2.jpg",
        publishedAt: "2021-02-03T05:33:19Z",
        content:
          "The Mate X2 is official\r\nHuawei will announce a new flagship foldable phone later this month, according to a Weibo post on the companys official page. Based on the name, the Mate X2 sounds like it sh… [+1016 chars]",
      },
      {
        source: {
          id: "associated-press",
          name: "Associated Press",
        },
        author: "Lisa Mascaro",
        title:
          "Biden, Yellen say GOP virus aid too small, Democrats push on - The Associated Press",
        description:
          "WASHINGTON (AP) — President Joe Biden panned a Republican alternative to his  $1.9 trillion COVID rescue plan  as insufficient as Senate Democrats pushed ahead, voting to launch a process that...",
        url:
          "https://apnews.com/article/joe-biden-financial-markets-janet-yellen-bills-coronavirus-pandemic-e5aa4373b5cf57f44ca21332c814b3af",
        urlToImage:
          "https://storage.googleapis.com/afs-prod/media/97368a17d7fd4e3ebca46afd2883f49e/3000.jpeg",
        publishedAt: "2021-02-03T05:22:03Z",
        content:
          "WASHINGTON (AP) President Joe Biden panned a Republican alternative to his $1.9 trillion COVID rescue plan as insufficient as Senate Democrats pushed ahead, voting to launch a process that could appr… [+5857 chars]",
      },
      {
        source: {
          id: "newsweek",
          name: "Newsweek",
        },
        author: "Daniel Villarreal",
        title:
          "Rachel Maddow Says Donald Trump's Impeachment Defense Gives the GOP a Catch-22 - Newsweek",
        description:
          "A legal brief from Trump's defense team states that he still thinks he is the president, leaving Senate Republicans to either agree or disagree, neither of which puts them in a comfortable position.",
        url:
          "https://www.newsweek.com/rachel-maddow-says-donald-trumps-impeachment-defense-gives-gop-catch-22-1566353",
        urlToImage:
          "https://d.newsweek.com/en/full/1723445/donald-trump-senate-impeachment-trial-defense-catch-22.jpg",
        publishedAt: "2021-02-03T05:03:23Z",
        content:
          'A brief submitted by the legal team of former President Donald Trump to defend him from charges of "inciting insurrection" in his upcoming Senate impeachment trial states that he still considers hims… [+3559 chars]',
      },
    ],
  };
  