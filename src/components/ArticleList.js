import React, { useEffect, useState, useRef, useCallback } from "react";
import {
  useGlobalDispatchContext,
  useGlobalStateContext,
} from "../context/globalContext";
import { Select, Layout } from "antd";
import { fetchNews } from "../api";

//static data
import { countries } from "../util";
//custom components
import Article from "./Article";
import Loader from "./Loader";
import ErrorComponent from "./ErrorComponent";

const { Option } = Select;
const { Content, Header } = Layout;
function ArticleList({ articles, propPage, propCountry }) {
  const pageSize = 10;
  const [state, setState] = useState({
    loading: articles.length == 0,
    totalArticles: 0,
    currentPage: propPage,
    hasError: false,
    hasMore: false,
    country: localStorage.getItem("prefered_country")
      ? localStorage.getItem("prefered_country")
      : "us",
  });
  const dispatch = useGlobalDispatchContext();
  const observer = useRef();
  const lastEl = useCallback(
    (node) => {
      if (state.loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          console.log("intersecting");
          if (state.hasMore)
            setState({
              ...state,
              currentPage: state.currentPage + 1,
            });
          else observer.current.observe(node);
        }
      });
      if (node) observer.current.observe(node);
    },
    [articles, state.hasMore]
  );
  const updateArticles = () => {
    // setState({ ...state });
    fetchNews(state.country, state.currentPage, pageSize).then((data) => {
      dispatch({
        type: "UPDATE_ARTICLES",
        value:
          articles.length > 0 ? articles.concat(data.articles) : data.articles,
      });
      // console.log(data.totalResults , state.currentPage * pageSize);
      setState({
        ...state,
        loading: false,
        totalArticles: data.totalResults,
        hasMore: data.totalResults > state.currentPage * pageSize,
      });
      dispatch({
        type: "UPDATE_PREV_PAGE",
        value: state.currentPage,
      });
      dispatch({
        type: "UPDATE_PREV_COUNTRY",
        value: state.country,
      });
    });
  };
  const onChangeCountry = (changedCountry) => {
    localStorage.setItem("prefered_country", changedCountry);
    dispatch({
      type: "UPDATE_ARTICLES",
      value: [],
    });
    
    setState({
      ...state,
      loading: true,
      country: changedCountry,
      currentPage: 1,
    });
  };

  useEffect(() => {
    console.log("state",state.country, state.currentPage);
    console.log("prop",propCountry,propPage);
    if (state.country != propCountry || state.currentPage != propPage)
      updateArticles();
  }, [state.country, state.currentPage]);
  return (
    <>
      <Header style={{ position: "sticky", zIndex: 1, width: "100%", top: 0 }}>
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Select Country"
          optionFilterProp="children"
          defaultValue={state.country}
          onChange={onChangeCountry}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {Object.entries(countries).map(([key, name]) => (
            <Option value={key} key={key}>
              {name}
            </Option>
          ))}
        </Select>
      </Header>
      <Content>
        {state.loading && <Loader />}

        {state.hasError && !state.loading && <ErrorComponent />}
        {!state.hasError && !state.loading && (
          <div className="article__container">
            {articles.map((articleProps, index) => {
              if (index + 1 === articles.length) {
                return (
                  <section ref={lastEl} key={index}>
                    <Article {...articleProps} />
                  </section>
                );
              } else {
                return (
                  <section key={index}>
                    <Article {...articleProps} />
                  </section>
                );
              }
            })}
          </div>
        )}
      </Content>
    </>
  );
}

export default ArticleList;
