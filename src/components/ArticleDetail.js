import React from "react";
import { Layout, Tag } from "antd";
import { formateDate } from "../util";
import { useGlobalStateContext } from "../context/globalContext";
import "../css/details.css";
const { Content, Header } = Layout;
function ArticleDetail() {
  const { selectedArticle } = useGlobalStateContext();
  const {
    urlToImage,
    title,
    description,
    url,
    source,
    publishedAt,
    author,
    content,
  } = selectedArticle;
  return (
    <>
      <Header />
      <Content>
        <div className="detail_article_container">
          <div className="detail_article_image">
            <img src={urlToImage} alt="" />
          </div>
          <div className="detail_article_content">
            <h1>{title}</h1>
            <h2>{description}</h2>
            <Tag color="#108ee9">
              <a href={url}>{source.name ? source.name : "source"}</a>
            </Tag>
            <time>{formateDate(publishedAt)} </time>
            {author && author != "" && (
              <>
                | <b>{author}</b>
              </>
            )}
            <p>{content}</p>
          </div>
        </div>
      </Content>
    </>
  );
}

export default ArticleDetail;
