import React, { Component } from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";
import { formateDate } from "../util";
import { useGlobalDispatchContext } from "../context/globalContext";
function Article(props) {
  const dispatch = useGlobalDispatchContext();
  const {author, publishedAt, title, description, urlToImage} = props;
  return (
    <Link
      to="/detail"
      onClick={() => {
        dispatch({
          type: "UPDATE_SELECTED_ARTICLE",
          load: props,
        });
      }}
    >
      <Card hoverable className="article__card">
        <div className="article__image">
          <img src={urlToImage} alt="" />
        </div>
        <h1 className="article__heading">{title}</h1>
        <p>{description}</p>
        <p className="secondary__info">
          {formateDate(publishedAt)} {author && `| ${author}`}
        </p>
      </Card>
    </Link>
  );
}

export default Article;
